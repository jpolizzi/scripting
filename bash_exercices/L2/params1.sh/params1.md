Écrire un script qui renvoie le nombre de paramètres avec lesquels il a été appelé. 

Exemple : 

./params1.sh un deux trois 

J'ai été appelé avec 3 arguments. 
