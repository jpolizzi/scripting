Exercice 9 : nombre_mystere.sh
Il faut deviner le nombre mystère. Celui-ci est compris entre 1 et 1000. Le joueur a dix essais.

Exemple :

Entrez un nombre entre 1 et 1000 : 500
Le nombre est plus grand.
Il vous reste 9 essais.
Entrez un nombre entre 1 et 1000 : 750
Le nombre est plus petit.
ntrez un nombre entre 1 et 1000 : 565
Vous avez gagné !

Exemple 2 :

Entrez un nombre entre 1 et 1000 : 500
...
Entrez un nombre entre 1 et 1000 : 10
Il vous reste 0 essais.
Vous avez perdu ! Le nombre mystère était 9.
