#!/bin/bash
nb=$((RANDOM%1000+1))
nbtry=15
while [ $nbtry -ge 1 ]
do
	read -p 'Devinez un nombre entre 1 et 1000 : ' rep
	if [ -z $rep ]; then
		echo -e "\n"
	else
		if [ $rep -gt $nb ]; then
			echo -e "le nb est plus petit.\n"
		elif [ $nb -gt $rep ]; then
			echo -e "le nb est plus grand.\n"
		else 
			echo -e "Félicitations, $nb et le bon nb trouvé !\n"
			exit 0
		fi
	fi
	nbtry=$(($nbtry-1))
	echo -e "$nbtry essais restants.\n"
done
echo -e "Perdu ! le nb mystère était $nb.\n"
