Écrire un script qui demande à l'utilisateur le chemin absolu de son home. L'utilisateur à 5 secondes pour entrer le chemin, sinon le script affiche "Trop tard !" 


Exemple : 

Chemin absolu de votre home : /home/matthieu 

Votre home se trouve ici : /home/matthieu 


Exemple 2 : 

Chemin absolu de votre home : 

Trop tard !
