#!/bin/bash

nb1=`ls "$1" | wc -w`
nb2=`ls "$2" | wc -w`

if [ $nb1 -gt $nb2 ]; then
	echo "Il y a $(($nb1-$nb2)) élément(s) de plus dans $1/"
elif [ $nb2 -gt $nb1 ]; then
	echo "Il y a $(($nb2-$nb1)) élément(s) de plus dans $2/"
else
	echo "Les deux dossiers $1/ et $2/ ont le même nb d'élements."
fi
