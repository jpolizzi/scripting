Exercice 12 : fichiers.sh
Écrire  un script qui crée dans le répertoire courant un répertoire nommé 'files' et qui quand on fait un ls -gG dessus donne le résultat suivant :

total 16
-rwxrwxrwx 1    0 janv.  2 03:04 file1
-r---w---x 2   10 nov.  26 15:45 file2
---S------ 1   20 mars   4 05:06 file3
d-wxr--r-x 2 4096 avril  5 06:07 file4
pr--r-xr-x 1    0 juil.  8 09:10 file5
-r---w---x 2   10 nov.  26 15:45 file6
lrwxrwxrwx 1    5 nov.  26 15:45 file7 -> file1

A la place du 26 novembre 15:45 vous aurez la date d'exécution du script. Le reste doit être identique.
