#!/bin/bash
mkdir files;cd files

touch -t 01020304 file1;chmod 777 file1

echo -n xxxxxxxxxx > file2
touch file2 file6;chmod 421 file2 file6

echo -n xxxxxxxxxxxx > file3;touch -t 03040506 file3;chmod 4000 file3

mkdir file4; touch -t 04050607 file4/; chmod 345 file4/

mkfifo -m 455 file5;touch -t 07080910 file5

ln file2 file6
ln -s file1 file7
cd ..;ls -Gg files/
