Écrire un script qui affiche son nom, puis le chemin par lequel il a été appelé, puis tout ses arguments un à un. 

Exemple : 

./params2.sh un deux trois quatre cinqs six sept huit neuf dix onze 

nom: params.sh 

chemin: . 

argument 1 : un 

argument 2 : deux 

argument 3 : trois 

argument 4 : quatre 

argument 5 : cinqs 

argument 6 : six 

argument 7 : sept 

argument 8 : huit 

argument 9 : neuf 

argument 10 : dix 

argument 11 : onze
