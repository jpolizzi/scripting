Exercice 13 : ls_path.sh
Affiche le contenu de tous les répertoires qui apparaissent dans le chemin absolue du répertoire courant.

./ls_path
/home/matthieu
bootcamp_bash  Desktop Documents Download Music Pictures Public Templates Videos
 
/home
matthieu
 
/
bin   cdrom  etc   ...
boot  dev    home  ...
