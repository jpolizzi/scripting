Écrire un script qui affiche l'emplacement du fichier  tutor. Vous aurez peut-être besoin de passer en root temporairement. 

Si le fichier n'est pas présent sur votre système, vous pouvez travailler sur n'importe quel fichier de plus de 26 lignes. 

A l'aide d'une unique ligne de commandes bash, affichez les 26 dernières lignes du fichier tutor, de la dernière à la première, écrites à l'envers, en supprimant les lignes vides, précédé du numéro de la ligne (la dernière ligne aura le numéro 20), le tout trié dans l'ordre alphabétique.
