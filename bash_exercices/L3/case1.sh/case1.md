Exercice 2 : case1.sh
Usage : case1.sh int

Écrire un script qui affiche "Mauvais" si int est entre 0 et 4, "Passable" entre 5 et 9, "Bien" entre 10 et 14, "Très bien" entre 15 et 19, "Parfait" si int vaut 20, et "WTF?!" autrement.

Vous ne pouvez pas utiliser de if.
