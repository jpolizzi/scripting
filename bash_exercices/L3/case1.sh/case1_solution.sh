#!/bin/bash
[ $# -ne 1 ] && echo "Usage `basename $0` 0<->20" && exit 1

case $1 in
	[0-4]) echo Mauvais;;
	[5-9]) echo Passable;;
	1[0-4]) echo Bien;;
	1[5-9]) echo Très bien;;
	20) echo Parfait;;
	*) echo "WTF?!";;
esac
