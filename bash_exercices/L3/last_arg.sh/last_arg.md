Écrire un script qui affiche le dernier argument avec lequel il est invoqué.

Exemple :

./last_arg.sh un deux trois
trois
